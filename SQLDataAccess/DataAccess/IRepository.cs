﻿using SQLDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLDataAccess.DataAccess
{
    public interface IRepository<T>
    {
        /// <summary>
        /// Method for creating a customer
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Create(T entity);
        
        /// <summary>
        /// Method for reading all the customer from database
        /// </summary>
        /// <returns></returns>
        List<T> GetAll();
        
        /// <summary>
        /// Method for getting a customer by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T FindById(int id);
        
        /// <summary>
        /// Method for updating a customer
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="id"></param>
        bool Update(T entity, int id);

        /// <summary>
        /// Method for deleting a customer
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(int id);

        
        /// <summary>
        /// Method for reading a list of customer based on the limit and offset
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        List<T> ReadPage(int limit, int offset);
    }
}
