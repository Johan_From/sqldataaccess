﻿using Microsoft.Data.SqlClient;
using SQLDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLDataAccess.DataAccess
{
    public class SQLClientCustomer : ICustomerRepository
    {
        /// <summary>
        /// Method for creaing a customer
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>True or false</returns>
        public bool Create(Customer customer)
        {
            bool success = false; // Set success to false

            string sql = File.ReadAllText("../../../SqlQueries/Create.sql"); // Read the .sql file
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString())) // Get connection
                {
                    conn.Open(); // Open the conneciton
                
                    // Set the command with the needed values
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Add Parameters
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@PhoneNumber", customer.PhoneNumber);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);

                        // Execture command with ternary statement, true or false
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return success;
        }

        /// <summary>
        /// Method for fetching all the customers
        /// </summary>
        /// <returns>Object for customer</returns>
        public List<Customer> GetAll()
        {
            List<Customer> customers = new List<Customer>(); // List of customers
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString())) // Connect
                {
                    conn.Open(); // Open connection 
                    string sql = File.ReadAllText("../../../SqlQueries/GetAllCustomers.sql"); // Open the .sql file

                    using (SqlCommand cmd = new SqlCommand(sql, conn)) 
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader()) // Excecute the reader with command
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer(); // New customer object

                                // Set values to customer object
                                customer.ID = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);

                                if (!reader.IsDBNull(3)) customer.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4)) customer.PostalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5)) customer.PhoneNumber = reader.GetString(5);

                                customer.Email = reader.GetString(6);

                                // Add the values
                                customers.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customers;
        }

        /// <summary>
        /// Method for fini
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Object for customer</returns>
        public Customer FindById(int id)
        {
            // New Customer object
            Customer customer = new Customer();
            try
            {
                // Get connection
                using(SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    // Open connection
                    conn.Open();
                    // Ge the query file
                    string sql = File.ReadAllText("../../../SqlQueries/FindById.sql");

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Add parameters to @Id
                        cmd.Parameters.AddWithValue("@Id", id);

                        using(SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Execute reder
                            while (reader.Read())
                            {
                                customer = new Customer();

                                // Set values
                                customer.ID = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);

                                if (!reader.IsDBNull(3)) customer.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4)) customer.PostalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5)) customer.PhoneNumber = reader.GetString(5);

                                customer.Email = reader.GetString(6);

                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customer;
        }

        /// <summary>
        /// Method for finding customer by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>List for customers</returns>
        public List<Customer> FindByName(string name)
        {
            // Use a list in case of multiple same names
            List<Customer> customerList = new List<Customer>();

            try
            {
                // Get connection
                using(SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    // Open connectio and read the query file
                    conn.Open();
                    string sql = File.ReadAllText("../../../SqlQueries/FindByName.sql");

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Add parameters
                        cmd.Parameters.AddWithValue("@Name", name);

                        using(SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Read
                            while (reader.Read())
                            {
                                // Add new customer object to list
                                customerList.Add(new Customer()
                                {
                                    ID = reader.GetInt32(0),
                                    FirstName = reader.GetString(1),
                                    LastName = reader.GetString(2),
                                    Country = reader.GetString(3),
                                    PostalCode = reader.GetString(4),
                                    PhoneNumber = reader.GetString(5),
                                    Email = reader.GetString(6)

                                });
                            }

                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customerList;
        }

        /// <summary>
        /// Method for updating a customer
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="id"></param>
        /// <returns>True or false</returns>
        public bool Update(Customer customer, int id)
        {
            // Boolean for checking if sucess or not
            bool success = false;
            try
            {
                // Read the query file
                string sql = File.ReadAllText("../../../SqlQueries/Update.sql");

                // Get connection
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    // Open the connection and adding the parameters
                    conn.Open();
                    using(SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@PhoneNumber", customer.PhoneNumber);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        cmd.Parameters.AddWithValue("@CustomerId", id);

                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return success;
        }

        /// <summary>
        /// Method for deleting
        /// </summary>
        /// <param name="id"></param>
        /// <returns>True or false</returns>
        public bool Delete(int id)
        {
            bool success = false;
            try
            {
                // Read the query file
                string sql = File.ReadAllText("../../../SqlQueries/Delete.sql");
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@Id", id);

                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return success;
        }

        /// <summary>
        /// Method for reading a page of customers with a limit and offset
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns>List of customers</returns>
        public List<Customer> ReadPage(int limit, int offset)
        {
            // Need a list for customer objects
            List<Customer> customerList = new List<Customer>();

            try
            {
                // Get the connection
                using(SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    // Open connection and reading the query file
                    conn.Open();
                    string sql = File.ReadAllText("../../../SqlQueries/ReadPage.sql");

                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Add parameters
                        cmd.Parameters.AddWithValue("@Offset", offset);
                        cmd.Parameters.AddWithValue("@Limit", limit);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Read
                            while (reader.Read())
                            {
                                // Add new Customer object to list
                                customerList.Add(new Customer()
                                {
                                    ID = reader.GetInt32(0),
                                    FirstName = reader.GetString(1),
                                    LastName = reader.GetString(2),
                                    Country = reader.GetString(3),
                                    PostalCode = reader.GetString(4),
                                    PhoneNumber = reader.GetString(5),
                                    Email = reader.GetString(6)

                                });
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customerList;
        }

        /// <summary>
        /// Get the countries of customers with a count
        /// </summary>
        /// <returns>List of customers</returns>
        public List<CustomerCountry> GetCustomersByCountry()
        {
            // Need a list to store the object of CustomerCountry
            List<CustomerCountry> customerCountry = new List<CustomerCountry>();

            try
            {
                // Read the query file
                string sql = File.ReadAllText("../../../SqlQueries/GetCountries.sql");

                // Get connection
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    // Open connection
                    conn.Open();

                    // New command
                    using(SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using(SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Rad
                            while (reader.Read())
                            {
                                // Add CustomerCountry object to list
                                customerCountry.Add(new CustomerCountry()
                                {
                                    Country = reader.GetString(1),
                                    Count = reader.GetInt32(0)
                                });
                                
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return customerCountry;
        }

        /// <summary>
        /// Method for getting customers by invoice
        /// </summary>
        /// <returns>List of CustomerSpenders</returns>
        public List<CustomerSpender> GetCustomerByInvoice()
        {
            // Need a list to store the object of CustomerSpender
            List<CustomerSpender> invoices = new List<CustomerSpender>();

            try
            {
                // Read the query file
                string sql = File.ReadAllText("../../../SqlQueries/GetSpenders.sql");

                // Get conneciton
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    // Open connction
                    conn.Open();

                    // New command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Read
                            while (reader.Read())
                            {
                                // New CustomerSpender object to list
                                invoices.Add(new CustomerSpender()
                                {
                                    FirstName = reader.GetString(1),
                                    LastName = reader.GetString(2),
                                    ID = reader.GetInt32(0),
                                    TotalMoneySpent = reader.GetDecimal(3)
                                });
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return invoices;
        }

        /// <summary>
        /// Method for getting the most played genre by a customer with id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of CustomerGernre</returns>
        public List<CustomerGenre> GetCustomerMostPlayedGenre(int id)
        {
            //List of Customer Genres
            List<CustomerGenre> genres = new List<CustomerGenre>();

            try
            {
                // Get the query file
                string sql = File.ReadAllText("../../../SqlQueries/GetGenres.sql");

                // Get connection
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    // Open connection
                    conn.Open();

                    // New command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Add parameters 
                        cmd.Parameters.AddWithValue("@Id", id);

                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Read
                            while (reader.Read())
                            {
                                // Add CustomerGenre object to list
                                genres.Add(new CustomerGenre()
                                {
                                    GenreCount = reader.GetInt32(0),
                                    GenreName = reader.GetString(1)
                                });
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return genres;

        }

    }

}
