﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLDataAccess.DataAccess
{
    public class ConnectionStringHelper
    {
        /// <summary>
        /// Method for connecting to database
        /// </summary>
        /// <returns>Connection string for connection</returns>
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder connectionStringBuilder = new()
            {
                DataSource = @"localhost\SQLEXPRESS", // Own sql server
                InitialCatalog = "Chinook", // Server name
                IntegratedSecurity = true,
                TrustServerCertificate = true
            };

            return connectionStringBuilder.ConnectionString;
        }
    }
}
