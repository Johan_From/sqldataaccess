﻿using SQLDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLDataAccess.DataAccess
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        /// <summary>
        /// Method for finding a customer by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        List<Customer> FindByName(string name);

        /// <summary>
        /// Method for finding countries
        /// </summary>
        /// <returns></returns>
        List<CustomerCountry> GetCustomersByCountry();

        /// <summary>
        /// Method for finding the top spenders
        /// </summary>
        /// <returns></returns>
        List<CustomerSpender> GetCustomerByInvoice();

        /// <summary>
        /// Method for finding the most played genre with ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        List<CustomerGenre> GetCustomerMostPlayedGenre(int id);
    }
}
