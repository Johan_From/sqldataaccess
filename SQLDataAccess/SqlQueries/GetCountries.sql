﻿/*
	Get the countries with count
*/

SELECT COUNT(CustomerId), Country 
FROM Customer 
GROUP BY Country 
ORDER BY 
COUNT(CustomerId) DESC