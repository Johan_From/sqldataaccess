﻿/*
	Get the genres with their specific ID
	JOINS InvoiceLine table, Track table and Genre Table
	Get's the count
*/

SELECT TOP 1 WITH TIES COUNT(G.GenreId)
	AS GenreCount, G.Name
FROM Invoice 
	AS IV
INNER JOIN InvoiceLine 
	AS IVL
ON IV.InvoiceId = IVL.InvoiceId
INNER JOIN Track
	AS T 
ON IVL.TrackId = T.TrackId
INNER JOIN Genre 
	AS G
ON T.GenreId = G.GenreId
	WHERE CustomerId = @Id
GROUP BY G.GenreId, G.Name
	ORDER BY COUNT(G.GenreId)
DESC
