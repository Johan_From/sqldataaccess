﻿/*
	Find a customer with their first or last name
*/

SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email 
FROM Customer 
WHERE FirstName LIKE @Name OR LastName LIKE @Name