﻿/*
	Find a customer by their ID
*/

SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email 
FROM Customer 
WHERE CustomerId = @id;