﻿/*
	Update an customer
*/

UPDATE Customer
SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @PhoneNumber, Email = @Email
WHERE CustomerId = @CustomerId