﻿/*
	Get the customers with an limit and offset
*/

SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email 
FROM Customer 
ORDER BY CustomerID OFFSET @Offset 
ROWS FETCH NEXT @Limit 
ROWS ONLY