﻿/*
	Creates a linking table
*/

CREATE TABLE SuperheroPowers(
	SuperheroID int FOREIGN KEY REFERENCES Superhero(ID),
	SuperpowerID int FOREIGN KEY REFERENCES Superpower(ID)
);