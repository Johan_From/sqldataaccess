﻿/*
	Inserting data into Superhero
*/

INSERT INTO Superhero VALUES(
	'Superman',
	'Clark Kent',
	'Krypton'
);

INSERT INTO Superhero VALUES(
	'Batman',
	'Bruce Wayne',
	'Gotham'
);

INSERT INTO Superhero VALUES(
	'Deadpool',
	'Wade Wilson ',
	'Canada'
);