﻿/*
	Insert data into the different tables
*/

INSERT INTO Superpower VALUES(
	'Fly',
	'Can fly really fast'
);

INSERT INTO Superpower VALUES(
	'Everything',
	'Batman can do everything because he is the best'
);

INSERT INTO Superpower VALUES(
	'Immortal',
	'Mutation sideeffect'
);

INSERT INTO Superpower VALUES(
	'Laser',
	'Shoot laser from eyes'
);


INSERT INTO SuperheroPowers 
	(SuperheroID, SuperpowerID)
VALUES
	(1 ,1);

INSERT INTO SuperheroPowers 
	(SuperheroID, SuperpowerID)
VALUES
	(2 ,2);

INSERT INTO SuperheroPowers 
	(SuperheroID, SuperpowerID)
VALUES
	(3 ,3);

INSERT INTO SuperheroPowers
	(SuperheroID, SuperpowerID)
VALUES
	(3 ,1);

INSERT INTO SuperheroPowers 
	(SuperheroID, SuperpowerID)
VALUES
	(2 ,4);