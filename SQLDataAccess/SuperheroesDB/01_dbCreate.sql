﻿/*
	Important Note: 
	If you put all nine queries in a single query file, use GO
															USE SuperheroesDb
	under the CREATE DATABASE SuperheroesDb
	
	If not:
	If you execute a single script at a time, specially when creating the tables, use USE SuperheroesDb above the other queries
*/

CREATE DATABASE SuperheroesDb;
GO
USE SuperheroesDb