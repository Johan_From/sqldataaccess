﻿/*
	Creates the three tables
*/

CREATE TABLE Superhero(
	ID int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	SuperheroName nvarchar(50) NOT NULL,
	Alias nvarchar(50) NULL,
	Origin nvarchar(50) NOT NULL
);

CREATE TABLE Assistant(
	ID int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	AssistantName nvarchar(50) NOT NULL
);

CREATE TABLE Superpower(
	ID int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	SuperpowerName nvarchar(50) NOT NULL,
	SuperpowerDescription nvarchar(50) NULL
);