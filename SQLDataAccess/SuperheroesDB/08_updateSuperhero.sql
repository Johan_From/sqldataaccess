﻿/*
	Update the superhero table with new values
*/
UPDATE 
	Superhero 
SET 
	SuperheroName = 'Eternal' 
WHERE 
	SuperheroName = 'Deadpool';