﻿/*
	Alter the table assistant with new foreign key
*/

ALTER TABLE
	Assistant
Add 
	SuperheroID int FOREIGN KEY (SuperheroID)
REFERENCES
	Superhero(ID) NOT NULL;