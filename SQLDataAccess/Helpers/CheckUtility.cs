﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLDataAccess.Helpers
{
    public class CheckUtility
    {
        
        /// <summary>
        /// Checking if the input number is valid
        /// </summary>
        /// <param name="strId"></param>
        /// <returns>True or false</returns>
        public static bool CheckIfInputIsNumber(string input)
        {
            // Bool for returning
            bool isParsed = false;

            if(input == string.Empty)
            {
                return isParsed;
            }

            // Discard later
            _ = int.TryParse(input, out int numericValue);
            

            if(numericValue == 0)
            {
                return isParsed;
            }

            isParsed = true;

            return isParsed;

        }

        /// <summary>
        /// Method for checking if input is valid string
        /// </summary>
        /// <param name="strName"></param>
        /// <returns>True or false</returns>
        public static bool CheckIfInputIsValidString(string input)
        {
            // Bool for returning
            bool isOk = false;

            if(input == string.Empty)
            {
                return isOk;
            }

            isOk = true;

            return isOk;
        }
    }
}
