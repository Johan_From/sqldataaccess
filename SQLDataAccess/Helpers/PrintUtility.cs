﻿using SQLDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLDataAccess.Helpers
{
    /// <summary>
    /// Mix between StringBuilder and Console.WriteLine()
    /// Console.WriteLine has overloads that accepts Console.ForegroundColor, StringBuilder does not have that kind of overload
    /// Using Enviroment.NewLine is safer than \n because it adapts after envrioment of computer (\n, \n\r, etc..) 
    /// </summary>
    public class PrintUtility
    {
        /// <summary>
        /// Method for printing out a single customer
        /// </summary>
        /// <param name="customer"></param>
        public static void PrintOutCustomers(Customer customer)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"ID: {customer.ID}");
            sb.AppendLine($"Fullname: {customer.FirstName} {customer.LastName}");
            sb.AppendLine($"Country: {customer.Country}");
            sb.AppendLine($"PostalCode: {customer.PostalCode}");
            sb.AppendLine($"Email: {customer.Email}");
            sb.AppendLine($"Phone number: {customer.PhoneNumber}");
            sb.AppendLine("-----------------------------------------");
            Console.WriteLine(sb);
        }

        /// <summary>
        /// Method for printing out customers from list
        /// </summary>
        /// <param name="customers"></param>
        public static void PrintOutCustomersFromList(List<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                // Calls the PrintOutCustomers method that onl takes one object
                PrintOutCustomers(customer);
            }
        }

        /// <summary>
        /// Method print the countries from list
        /// </summary>
        /// <param name="countries"></param>
        public static void PrintCountries(List<CustomerCountry> countries)
        {
            foreach (CustomerCountry customer in countries)
            {
                Console.Write("Country: ", Console.ForegroundColor = ConsoleColor.White);
                Console.WriteLine($"{customer.Country} ", Console.ForegroundColor = ConsoleColor.Green);
                Console.Write("Count: ", Console.ForegroundColor = ConsoleColor.White);
                Console.WriteLine($"{customer.Count} {Environment.NewLine}", Console.ForegroundColor = ConsoleColor.Green);
            }

            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// Method for printing total spending
        /// </summary>
        /// <param name="spender"></param>
        public static void PrintInvoices(List<CustomerSpender> spender)
        {
            foreach (CustomerSpender customer in spender)
            {
                Console.Write("Fullname: ", Console.ForegroundColor = ConsoleColor.White);
                Console.WriteLine($"{customer.FirstName} {customer.LastName} ", Console.ForegroundColor = ConsoleColor.Green);
                Console.Write("ID: ", Console.ForegroundColor = ConsoleColor.White);
                Console.WriteLine($"{customer.ID} ", Console.ForegroundColor = ConsoleColor.Green);
                Console.Write("Total Money Spent: ", Console.ForegroundColor = ConsoleColor.White);
                Console.WriteLine($"{customer.TotalMoneySpent}$ {Environment.NewLine}", Console.ForegroundColor = ConsoleColor.Green);
            }

            Console.ForegroundColor = ConsoleColor.White;

        }

        /// <summary>
        /// Method for printing genres
        /// </summary>
        /// <param name="genre"></param>
        public static void PrintGenre(List<CustomerGenre> genre)
        {
            foreach (CustomerGenre customer in genre)
            {
                Console.Write("Genre name: ", Console.ForegroundColor = ConsoleColor.White);
                Console.WriteLine($"{customer.GenreName} ", Console.ForegroundColor = ConsoleColor.Green);
                Console.Write("Count: ", Console.ForegroundColor = ConsoleColor.White);
                Console.WriteLine($"{customer.GenreCount} {Environment.NewLine}", Console.ForegroundColor = ConsoleColor.Green, Environment.NewLine); 
            }

            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
