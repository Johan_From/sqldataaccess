﻿using SQLDataAccess.DataAccess;
using SQLDataAccess.Main;
using System.Text;

namespace SQLDataAccess
{
    class Program
    {
        // List for menu options
        public static List<Options>? options;

        /// <summary>
        /// Program runs from here
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            // Instanciate the SQLClient
            ICustomerRepository customerRepository = new SQLClientCustomer();

            options = new()
            {
                new Options("  1) Show all customers",() => MethodHandler.ShowALLCustomers(customerRepository)),
                new Options("  2) Create customer (Finished inputs)", () => MethodHandler.CreateCustomer(customerRepository)),
                new Options("  3) Update customer (Finished inputs)", () => MethodHandler.UpdateUser(customerRepository)),
                new Options("  4) Delete a customer by Id", () => MethodHandler.DeleteCustomer(customerRepository)),
                new Options("  5) Find customer by id", () => MethodHandler.FindCustomerById(customerRepository)),
                new Options("  6) Find customer by name", () => MethodHandler.FindCustomerByName(customerRepository)),
                new Options("  7) Read a page of customers", () => MethodHandler.ReadPage(customerRepository)),
                new Options("  8) Get customer by country", () => MethodHandler.GetCustomerByCountry(customerRepository)),
                new Options("  9) Get customer by invoices", () => MethodHandler.GetCustomerByInvoice(customerRepository)),
                new Options("  10) Get cutomers most popular genre", () => MethodHandler.GetCustomerGenre(customerRepository)),
                new Options("  0) Exit", () => CloseApplication())
            };

            // Menu index starts at 0
            int index = 0;
            int menuLenght = options.Count();

            // Print the menu with index
            WriteMenu(options, options[index]);

            ConsoleKeyInfo keyinfo;

            // D0 while loop for handling the menu change
            do
            {
                keyinfo = Console.ReadKey();

                // Handle each key input (down arrow will write the menu again with a different selected item)
                if (keyinfo.Key == ConsoleKey.DownArrow) // 
                {
                    // While pressing down, add an index
                    if (index + 1 < options.Count)
                    {
                        index++;
                        WriteMenu(options, options[index]);
                    }
                    // If you get to the last and press again, set index to 0 and move up
                    else if(index == (menuLenght - 1) && keyinfo.Key == ConsoleKey.DownArrow)
                    {
                        index = 0;
                        WriteMenu(options, options[index]);
                    }
                    
                }
                if (keyinfo.Key == ConsoleKey.Spacebar)
                {
                    WriteMenu(options, options[index]);
                }
                if (keyinfo.Key == ConsoleKey.UpArrow)
                {
                    // If you press up
                    if (index - 1 >= 0)
                    {
                        index--;
                        WriteMenu(options, options[index]);
                    }
                    // If you press up and get to index 0, set index to 10 and move to last option
                    else if(index == 0 && keyinfo.Key == ConsoleKey.UpArrow)
                    {
                        index = menuLenght - 1;
                        WriteMenu(options, options[index]);
                    }
                }
                // Handle different action for the option when pressed enter
                if (keyinfo.Key == ConsoleKey.Enter)
                {
                    options[index].Selected.Invoke();
                    index = 0; // Index becomes 0
                }
            }
            while (keyinfo.Key != ConsoleKey.X);

            Console.ReadKey();

        }   

        /// <summary>
        /// Method for handling the menu options and printing out the menu
        /// </summary>
        /// <param name="options"></param>
        /// <param name="selectedOption"></param>
        static void WriteMenu(List<Options> options, Options selectedOption)
        {
            Console.Clear();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("  -------------------------------------");
            sb.AppendLine("  |     SQL Data Access Project       |");
            sb.AppendLine("  |  By Armin Ljajic and Johan From   |");
            sb.AppendLine("  |-----------------------------------|");
            sb.AppendLine("  |  Presented by Noroff Accelerate   |");
            sb.AppendLine("  -------------------------------------");
            Console.WriteLine(sb.ToString(), Console.ForegroundColor = ConsoleColor.Cyan);
            
            // Loop through the menu options
            foreach (Options option in options)
            {
                // Based on which one is selected, highlight with color and >
                if (option == selectedOption)
                {
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write("> ");
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write(" ");
                }
                // Print the option name
                Console.WriteLine(option.Name);
            }
        }

        // Method for closeing the application
        static void CloseApplication()
        {
            Console.WriteLine("Goodbye");
            Console.ForegroundColor = ConsoleColor.White;
            Environment.Exit(0);
        }
    }
}