﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLDataAccess.Main
{
    /// <summary>
    /// Class for handling menu chocies
    /// </summary>
    public class Options
    {
        public string Name { get; } // Name of menu option
        public Action Selected { get; } // The specific method
        public Options(string name, Action selected)
        {
            Name = name;
            Selected = selected;
        }
    }
}
