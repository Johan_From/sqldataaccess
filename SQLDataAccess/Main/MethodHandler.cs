﻿using SQLDataAccess.DataAccess;
using SQLDataAccess.Helpers;
using SQLDataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLDataAccess.Main
{
    /// <summary>
    /// Class for handling input from user
    /// </summary>
    public class MethodHandler
    {
        public static void ShowALLCustomers(ICustomerRepository customerRepository)
        {
            Console.Clear();

            // Get all customers
            List<Customer> customers = customerRepository.GetAll();

            // Checks if customers is 0 or not
            if (customers.Count == 0) Console.WriteLine("Could not find any customer!");
            if (customers.Count != 0)
            {
                foreach (Customer customer in customers)
                {
                    PrintUtility.PrintOutCustomers(customer);
                }
            }

            Console.WriteLine();
            Console.WriteLine("Press spacebar to continue...");
        }

        /// <summary>
        /// Method for creating a customer
        /// </summary>
        /// <param name="customerRepository"></param>
        public static void CreateCustomer(ICustomerRepository customerRepository)
        {
            Console.Clear();

            // New Customer object
            Customer customer = new Customer()
            {
                FirstName = "Johan",
                LastName = "From",
                Country = "Sweden",
                PostalCode = "22343",
                PhoneNumber = "0404873882",
                Email = "johan@gmail.se"
            };

            // Boolean for checking if creating went ok 
            bool success = customerRepository.Create(customer);

            if (success == true) Console.WriteLine("A user has been created");

            if (success != true) Console.WriteLine("Could not create a user");

            Console.WriteLine();
            Console.WriteLine("Press spacebar to continue...");

        }

        /// <summary>
        /// Method for reading a page
        /// </summary>
        /// <param name="customerRepository"></param>
        public static void ReadPage(ICustomerRepository customerRepository)
        {
            Console.Clear();

            // Limit
            Console.Write("Enter a limit: ");
            string? limit = Console.ReadLine();

            // Offset
            Console.Write("Enter an offset: ");
            string? offset = Console.ReadLine();

            // While input is not okay
            while (CheckUtility.CheckIfInputIsNumber(limit) == false || CheckUtility.CheckIfInputIsNumber(offset) == false)
            {
                Console.Write("Please enter a correct ID: ");
                limit = Console.ReadLine();
                offset = Console.ReadLine();
                Console.WriteLine("");
            }

            // Put repsonse from repo to list
            List<Customer> customerList = customerRepository.ReadPage(int.Parse(limit), int.Parse(offset));

            // Checking the result
            if (customerList.Count == 0) Console.WriteLine($"Could not find customers based on limit {limit} and offset {offset}!");

            if (customerList.Count != 0) PrintUtility.PrintOutCustomersFromList(customerList);

            Console.WriteLine();
            Console.WriteLine("Press spacebar to continue...");

        }

        /// <summary>
        /// Method for finding a customer by id
        /// </summary>
        /// <param name="customerRepository"></param>
        public static void FindCustomerById(ICustomerRepository customerRepository)
        {

            Console.Clear();

            // Id
            Console.Write("Enter the id of the cutomer you want to find: ");
            string? strId = Console.ReadLine();

            // While id input is not okay
            while (CheckUtility.CheckIfInputIsNumber(strId) == false)
            {
                Console.Write("Please enter a correct ID: ");
                strId = Console.ReadLine();
                Console.WriteLine("");
            }

            Console.WriteLine("");

            // Get the response from repo
            Customer customer = customerRepository.FindById(int.Parse(strId));

            // Check if Id is okay
            if (customer.ID == 0) Console.WriteLine($"No customer with ID {strId}!");

            if (customer.ID != 0) PrintUtility.PrintOutCustomers(customer);

            Console.WriteLine();
            Console.WriteLine("Press spacebar to continue...");
        }

        /// <summary>
        /// Method for finding a customer by name
        /// </summary>
        /// <param name="customerRepository"></param>
        public static void FindCustomerByName(ICustomerRepository customerRepository)
        {
            Console.Clear();

            // Get the name or last name
            Console.Write("Enter the name of the customer you want to find: ");
            string? strName = Console.ReadLine();

            // Checking if input is valid
            while (CheckUtility.CheckIfInputIsValidString(strName) == false)
            {
                Console.Write("Please enter a correct name: ");
                strName = Console.ReadLine();
                Console.WriteLine("");
            }

            // Response from repo to list
            List<Customer> customers = customerRepository.FindByName(strName);

            Console.WriteLine("");
            // Checks the count of response
            if (customers.Count == 0) Console.WriteLine("There was no customer/s with that name!");
            if (customers.Count != 0) PrintUtility.PrintOutCustomersFromList(customers);
           

            Console.WriteLine();
            Console.WriteLine("Press spacebar to continue...");
        }

        /// <summary>
        /// Method for updating a user
        /// </summary>
        /// <param name="customerRepository"></param>
        public static void UpdateUser(ICustomerRepository customerRepository)
        {
            Console.Clear();

            // New customer object
            Customer customer = new Customer()
            {
                FirstName = "Johan",
                LastName = "Ljajic",
                Country = "Sweden",
                PostalCode = "22343",
                PhoneNumber = "0404873882",
                Email = "johanljajic@gmail.se"
            };

            // Updating user as a bool
            bool success = customerRepository.Update(customer, 56);

            // Checks if it went okey
            if (success == true) Console.WriteLine($"Customer with name {customer.FirstName} {customer.LastName} has been updated!");

            if (success != true) Console.WriteLine($"Customer with name {customer.FirstName} {customer.LastName} could not be updated!");

            Console.WriteLine();
            Console.WriteLine("Press spacebar to continue...");
        }

        public static void DeleteCustomer(ICustomerRepository customerRepository)
        {
            Console.Clear();
            Console.WriteLine("NOTE: If customer exists in different parts of the database, it won't delete because of constraints!");
            Console.WriteLine("");

            // Id
            Console.Write("Enter the id of the cutomer you want to delete: ");
            string? ID = Console.ReadLine();

            // While id input is not okay
            while (CheckUtility.CheckIfInputIsNumber(ID) == false)
            {
                Console.Write("Please enter a correct ID: ");
                ID = Console.ReadLine();
                Console.WriteLine("");
            }

            Console.WriteLine("");

            bool success = customerRepository.Delete(int.Parse(ID));

            if (success == false) Console.WriteLine($"Could not delete customer with ID {ID}!", Console.ForegroundColor = ConsoleColor.Red);
            if (success == true) Console.WriteLine($"Customer ID {ID} has been deleted!");

            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("");
            Console.WriteLine("Press spacebar to continue...");
        }

        /// <summary>
        /// Method for getting the countries
        /// </summary>
        /// <param name="customerRepository"></param>
        public static void GetCustomerByCountry(ICustomerRepository customerRepository)
        {
            Console.Clear();

            // Put the method inside the print utility becuase it returns a list
            PrintUtility.PrintCountries(customerRepository.GetCustomersByCountry());

            Console.WriteLine("Press any button to continue...");
        }

        /// <summary>
        /// Method for getting the invoices
        /// </summary>
        /// <param name="customerRepository"></param>
        public static void GetCustomerByInvoice(ICustomerRepository customerRepository)
        {
            Console.Clear();

            // Put the method inside the print utility becuase it returns a list
            PrintUtility.PrintInvoices(customerRepository.GetCustomerByInvoice());

            Console.WriteLine();
            Console.WriteLine("Press spacebar to continue...");
        }

        /// <summary>
        /// Method for getting the customer genre
        /// </summary>
        /// <param name="customerRepository"></param>
        public static void GetCustomerGenre(ICustomerRepository customerRepository)
        {
            Console.Clear();

            // Enter id
            Console.Write("Enter the id of the customer you want to find: ");
            string? strId = Console.ReadLine();

            // While not okay
            while (CheckUtility.CheckIfInputIsNumber(strId) == false)
            {
                Console.Write("Please enter a correct ID: ");
                strId = Console.ReadLine();
                Console.WriteLine("");
            }

            Console.WriteLine("");

            // Response from repo is a list
            List<CustomerGenre> genre = customerRepository.GetCustomerMostPlayedGenre(int.Parse(strId));

            // Check there was a response
            if (genre.Count == 0) Console.WriteLine($"Could not find the genres with ID {strId}");

            if (genre.Count != 0) PrintUtility.PrintGenre(genre);

            Console.WriteLine();
            Console.WriteLine("Press spacebar to continue...");

        }
    }
}
