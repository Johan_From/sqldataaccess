﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLDataAccess.Models
{
    public class CustomerGenre
    {
        // Getters and Setters fr Customer genre
        public int GenreCount { get; set; }
        public string GenreName { get; set; }
    }
}
