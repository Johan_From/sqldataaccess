﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLDataAccess.Models
{
    public class CustomerSpender
    {
        // Getters and setters for spending
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ID { get; set; }
        public decimal TotalMoneySpent { get; set; }
    }
}
