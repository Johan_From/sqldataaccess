﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLDataAccess.Models
{
    public class CustomerCountry
    {
        // Getters and Setters for Customer country
        public string Country { get; set; }
        public int Count { get; set; }
    }
}
