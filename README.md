<h1>SQL Data Access</h1>
An C# console application that focuses on using the SQL Client library. The purpose of this console application is to interact with a database, be able to GET, UPDATE, INNER JOIN with much more while using the repository pattern. 

<h2>General Information</h2>
<ul>
<li>The app written in C# (.NET 6) and utalizes the SQL Client library.</li>
<li>The app's purpose is use classes, interfaces, repositories and much more.</li>
<li>The app's purpose is to understand sql and how to communicate with SQLExpress</li>
<li>To develop this application, pair programming was being utalized and <a href="https://marketplace.visualstudio.com/items?itemName=MS-vsliveshare.vsls-vs" target="_blank">Live Share</a></li>

<h2>Project Information</h2>

This project is split into two parts, one for writing only sql and one for creating a console application with SQL Client. The first part is the folder named ```SuperheroesDB```
and the second part touches the rest of the solution. NOTE: Read the first sql file ```01_dbCreate.sql``` comment for more information about running the queries.

<h2>Technologies Used</h2>
<li>C# (.NET 6) </li>
<li>SQL Client (NuGet package)</li>
<li>Microsoft SQL Server 2019 Express</li>
<li>Microsoft SQL Server Management Studio 18</li>

<h2>Setup</h2>
<p>To get the console application started started follow the steps below:</p>


1. This application was written with Visual Studio 2022 
2. Clone this repository ```https://gitlab.com/Johan_From/sqldataaccess.git```
3. Find ```.sln``` file and double click it
4. To run the application press F5 or click the green arrow at the top

<h2>Notes</h2>
To see how the database looks and want to mess around with it, you'll need to install the Microsoft SQL Server Management Studio <a href="https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver16" target="_blank">SSMS</a> and <a href="https://www.microsoft.com/en-us/Download/details.aspx?id=101064" target="_blank">SQL Express</a>. To get your own database upp and running, follow these steps below after you have installed the above: 
<p></p>

1. Open SSMS
2. Login in with server type ```Database Engine```, server name ```localhost/SQLEXPRESS``` and authentication ```Windows Authentication```
3. Click the button ```New Query```
4. Copy the content of the file ```Chinook_SqlServer_AutoIncrementPKs.sql``` found in this repository and paste it into the query screen
5. Press the button ```Execute```
6. Press the button up to the left ↻ (F5 refresh)
7. Now the tables have been created

<h2>Collaborators</h2>
<ul>
<li><a href="https://gitlab.com/Johan_From" target="_blank">Johan From</a></li>
<li><a href="https://gitlab.com/ArminExperis" target="_blank">Armin Ljajic</a></li>
</ul>
